//
//  ViewController.h
//  WebViewDemo
//
//  Created by Iceskysl on 1/24/13.
//  Copyright (c) 2013 1sters. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>
{
    UIWebView *webView;
    UIActivityIndicatorView *activityIndicator;
}
@end
