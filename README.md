### 简介
UIWebView是iOS sdk中一个最常用的控件。是内置的浏览器控件，我们可以用它来浏览网页、打开文档等等。这篇文章我将使用这个控件，载入指定url的网页（这里用的是http://www.eoe.cn）;
为了让体验更好一点，我们将加入一个在UIActivityIndicatorView，UIActivityIndicatorView实例提供轻型视图，这些视图显示一个标准的旋转进度轮。

### 创建工程
首选我们创建工程，运行XCode，新建一个Single View Application，命名为WebViewDemo；

### 编写.h文件
首先在ViewController.h添加需要的成员变量，参考代码 **ViewController.h**

### 编写.m文件
然后变现其实现文件，参考 **ViewController.m**

### 最后是截图
1.载入进度图
2.日志显示图
3.效果图


